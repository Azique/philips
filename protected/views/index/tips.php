<!doctype html>
<html>
<head>

    <meta charset="utf-8">
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->

    <title>Полезные советы</title>

    <meta name="description" content="">
    <meta name="keywords" content="">

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/reset.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/fancybox/jquery.fancybox-1.3.4.css"  rel="stylesheet"/>
	
    <!--[if lte IE 9]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" rel="stylesheet" />
    <![endif]-->
    <!--[if lte IE 7]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie7.css" rel="stylesheet" />
    <![endif]-->
    <!--[if lte IE 8]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie8.css" rel="stylesheet" />
    <![endif]-->

	<script type="text/javascript">
	var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-3029220-4']);
	  _gaq.push(['_setCampSourceKey', 'origin']);
	  _gaq.push(['_trackPageview']);
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';


		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

</head>
<body>
<div class="b-body">
		<div class="b-menu">
			<ul class="b-menu-list">
				<li class="b-header__logo-block">
					<a target="_blank" href="http://www.philips.ru" class="b-header__logo"></a>
				</li>
				<li class="b-menu-item test-drive active">
					<a href="testdrive" class="b-menu-item__href b-menu-item__href_violet">акции</a>
				</li>
				<li class="b-menu-item five-reasons">
					<a href="index" class="b-menu-item__href">пять<br>причин</a>
				</li>
				<li class="b-menu-item generator">
					<a href="whatis" class="b-menu-item__href">что такое парогенератор?</a>
				</li>
				<li class="b-menu-item technology">
					<a href="technology" class="b-menu-item__href b-menu-item__href_violet">технология optimal temp</a>
				</li>
				<li class="b-menu-item how-choose">
					<a href="choose" class="b-menu-item__href">как<br>выбрать?</a>
				</li>
				<li class="b-menu-item useful-help">
					<a href="tips" class="b-menu-item__href">полезные советы</a>
				</li>
				<li class="b-menu-item user-feedback">
					<a href="feedback" class="b-menu-item__href">отзывы пользователей</a>
				</li>
				<li class="b-menu-item promo-actions">
					<a target="_blank" href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerlaunch" class="b-menu-item__href">Купить</a>
					<div class="b-menu-flags">
						<a href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive?utm_source=steamerlaunch&amp;utm_medium=sitelinks&amp;utm_campaign=steamerlaunch" target="_blank" class="b-menu__flag rus"></a>
						<a href="http://www.philips.ru/e/promo/promotions/general/byshops.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive" target="_blank" class="b-menu__flag kaz"></a>
						<a href="http://www.philips.ru/e/promo/promotions/general/kzshops.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive" target="_blank" class="b-menu__flag bel"></a>
					</div>
				</li>
			</ul>
			<span class="b-delimeter"></span>
		</div>
    <div class="b-content__over b-whatis-content">
        <div class="b-tips-content__over">
            <h1 class="b-whatis__title b-tips__title">Полезные советы</h1>
            <div class="b-tips-video">
                <div class="b-tips__video video1" data-href="http://www.youtube.com/embed/Bm_szTJgNu4">
                    <iframe width="640" height="390" src="http://www.youtube.com/embed/Bm_szTJgNu4?wmode=transparent" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="b-tips__video video2" data-href="http://www.youtube.com/embed/koM77Pz7Su4">
                    <iframe width="640" height="390" src="http://www.youtube.com/embed/koM77Pz7Su4?wmode=transparent" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="b-tips__video video3" data-href="http://www.youtube.com/embed/9j2Wb6lVZtU">
                    <iframe width="640" height="390" src="http://www.youtube.com/embed/9j2Wb6lVZtU?wmode=transparent" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="b-tips__video video4" data-href="http://www.youtube.com/embed/7QqMe85S89c">
                    <iframe width="640" height="390" src="http://www.youtube.com/embed/7QqMe85S89c?wmode=transparent" frameborder="0" allowfullscreen></iframe>
                </div>
            </div><!--
            --><div class="b-tips-videos-block">
                <a href="#" class="b-tips-videos-block__prev"></a>
                <div class="b-tips-videos-block__over">
                    <ul class="b-tips-videos-pagionation">
                        <li class="b-tips-videos-pagination__item">
                            <a href="#" class="b-tips-videos-pagination__item-href" data-href="1">
                                <img class="b-tips-videos-pagination__item-img" src="images/11.png">
                                <p class="b-tips-videos-pagination__item-title">Парогенераторы Philips PerfectCare c технологией OptimalTemp</p>
                                <p class="b-tips-title-on-change">Парогенераторы Philips PerfectCare&nbsp;<br>c технологией OptimalTemp</p>
                                <p class="b-tips-text-on-change">Как работает парогенератор, почему он удобнее и эффективнее утюга, в чем его конкретные преимущества и достоинства?&nbsp;<br><br>Действительно ли гладить с ним в 2 раза быстрее? И почему?&nbsp;<br><br>В этом видео вы получите ответы на все вопросы и своими глазами увидите парогенератор Philips в действии. Особенно впечатляет технология OptimalTemp, которая обеспечивает максимальную подачу пара и в то же время регулирует температуру подошвы. Результат – вы можете забыть о настройках температуры. У парогенератора только одна настройка, и она идеально подходит для всех тканей.</p>
				    <p class="b-tips-text-share-on-change">Как работает парогенератор, почему он удобнее и эффективнее утюга, в чем его конкретные преимущества и достоинства?</p>
                            </a>
                        </li>
                        <li class="b-tips-videos-pagination__item">
                            <a href="#" class="b-tips-videos-pagination__item-href" data-href="2">
                                <img class="b-tips-videos-pagination__item-img" src="images/12.png">
                                <p class="b-tips-videos-pagination__item-title">От утюга к парогенератору</p>
                                <p class="b-tips-title-on-change">От утюга к парогенератору</p>
                                <p class="b-tips-text-on-change">Почему новое поколение парогенераторов объективно лучше предыдущих? Конечно, подошва утюга старого поколения, нагретая до 250 градусов, разгладит некоторые ткани. Но повреждения нежной материи, такой, как шелк, в данном случае неизбежны. Парогенераторы Philips позволяют поддерживать невысокую температуру подошвы за счет подачи пара. А это означает быстрое и эффективное глажение без риска испортить любимые вещи!</p>
				    <p class="b-tips-text-share-on-change">Почему новое поколение парогенераторов объективно лучше предыдущих?</p>
                            </a>
                        </li>
                        <li class="b-tips-videos-pagination__item">
                            <a href="#" class="b-tips-videos-pagination__item-href" data-href="3">
                                <img class="b-tips-videos-pagination__item-img" src="images/3.jpg">
                                <p class="b-tips-videos-pagination__item-title">Новый парогенератор Philips PerfectCare Pure</p>
                                <p class="b-tips-title-on-change">Новый парогенератор Philips PerfectCare Pure</p>
                                <p class="b-tips-text-on-change">Хотите узнать, что внутри самой современной и продвинутой системы глажения для дома? В этом ролике про новый парогенератор Philips PerfectCare Pure рассказывается, что именно делает это устройство эффективным. Оно безопасно для всех видов тканей, 1,5 литровый резервуар позволяет гладить до 2,5 часов, а вода автоматически очищается от накипи.</p>
				    <p class="b-tips-text-share-on-change">Хотите узнать, что внутри самой современной и продвинутой системы глажения для дома?</p>
                            </a>
                        </li>
                        <li class="b-tips-videos-pagination__item">
                            <a href="#" class="b-tips-videos-pagination__item-href" data-href="4">
                                <img class="b-tips-videos-pagination__item-img" src="images/4.jpg">
                                <p class="b-tips-videos-pagination__item-title">Бесшумный парогенератор Philips PerfectCare Silence</p>
                                <p class="b-tips-title-on-change">Бесшумный парогенератор Philips PerfectCare Silence</p>
                                <p class="b-tips-text-on-change">Парогенератор Philips PerfectCare Silence отличается от других удивительной бесшумностью. Технологи Philips звукоизолировали это устройство на нескольких уровнях. Получился действительно тихий парогенератор, который не будет мешать вам общаться с семьей или смотреть любимый фильм во время глажки. А еще PerfectCare Silence обладает всеми преимуществами парогенератора нового поколения: мощностью, эффективностью, легкостью и комфортом использования.</p>
				    <p class="b-tips-text-share-on-change">Парогенератор Philips PerfectCare Silence отличается от других удивительной бесшумностью.</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <a href="#" class="b-tips-videos-block__next"></a>
            </div>
            <div class="b-tips-left-block">
                <div class="b-tips-share">
                    <a href="#" class="b-tips-share__vk"></a>
                    <a href="#" class="b-tips-share__fb"></a>
                </div>
                <p class="b-tips-change__title">Парогенераторы Philips PerfectCare&nbsp;<br>c технологией OptimalTemp</p>
                <p class="b-tips-change__text">Как работает парогенератор, почему он удобнее и эффективнее утюга, в чем его конкретные преимущества и достоинства?&nbsp;<br><br>Действительно ли гладить с ним в 2 раза быстрее? И почему?&nbsp;<br><br>В этом видео вы получите ответы на все вопросы и своими глазами увидите парогенератор Philips в действии. Особенно впечатляет технология OptimalTemp, которая обеспечивает максимальную подачу пара и в то же время регулирует температуру подошвы. Результат – вы можете забыть о настройках температуры. У парогенератора только одна настройка, и она идеально подходит для всех тканей.</p>
		  <p class="b-tips-change__text-hide">Как работает парогенератор, почему он удобнее и эффективнее утюга, в чем его конкретные преимущества и достоинства?</p>
            </div>
        </div>
    </div>
    <div class="b-whatis-footer">
        <div class="b-whatis-footer__clouds"></div>
        <ul class="b-footer-menu">
            <li class="b-footer-item">
                <a href="index" class="b-footer-item__href">Пять причин</a>
            </li>
            <li class="b-footer-item">
                <a href="whatis" class="b-footer-item__href">Что такое парогенератор?</a>
            </li>
            <li class="b-footer-item">
                <a href="technology" class="b-footer-item__href">Технология Optimal Temp</a>
            </li>
            <li class="b-footer-item b-footer-item__spacer_parent">
                <a href="choose" class="b-footer-item__href b-footer-item__spacer">Как выбрать?</a>
            </li>
            <li class="b-footer-item">
                <a href="tips" class="b-footer-item__href active">Полезные советы</a>
            </li>
            <li class="b-footer-item">
                <a href="feedback" class="b-footer-item__href">Отзывы пользователей</a>
            </li>
            <li class="b-footer-item b-footer-item__last">
                <a target="_blank" href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerlaunch" class="b-footer-item__href">Промо акции</a>
            </li>
        </ul>
    </div>
</div>
<div class="popup_mail">
	<div class="popup">
    	<a target="_blank" href="http://www.shop.philips.ru/optimaltemp/technology/#innovations"><div class="hand1"></div></a>
        <div class="close">&#215;</div>
    </div><!-- popup -->
    <div class="girl"></div>
    <div class="hand2"></div>
</div><!-- popup_mail -->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/mousewheel.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/css3-mediaqueries.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.backgroundpos.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/share.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
</body>
</html>