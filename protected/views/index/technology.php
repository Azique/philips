<!doctype html>
<html>
<head>

    <meta charset="utf-8">
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->

    <title>Технология OptimalTemp</title>

    <meta name="description" content="">
    <meta name="keywords" content="">

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/reset.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/fancybox/jquery.fancybox-1.3.4.css"  rel="stylesheet"/>
	
    <!--[if lte IE 9]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" rel="stylesheet" />
    <![endif]-->
    <!--[if lte IE 7]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie7.css" rel="stylesheet" />
    <![endif]-->
    <!--[if lte IE 8]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie8.css" rel="stylesheet" />
    <![endif]-->

	<script type="text/javascript">
	var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-3029220-4']);
	  _gaq.push(['_setCampSourceKey', 'origin']);
	  _gaq.push(['_trackPageview']);
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';


		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

</head>
<body>
<div class="b-body">
   <div class="b-clouds"></div>
		<div class="b-menu">
			<ul class="b-menu-list">
				<li class="b-header__logo-block">
					<a target="_blank" href="http://www.philips.ru" class="b-header__logo"></a>
				</li>
				<li class="b-menu-item test-drive active">
					<a href="testdrive" class="b-menu-item__href b-menu-item__href_violet">акции</a>
				</li>
				<li class="b-menu-item five-reasons">
					<a href="index" class="b-menu-item__href">пять<br>причин</a>
				</li>
				<li class="b-menu-item generator">
					<a href="whatis" class="b-menu-item__href">что такое парогенератор?</a>
				</li>
				<li class="b-menu-item technology">
					<a href="technology" class="b-menu-item__href b-menu-item__href_violet">технология optimal temp</a>
				</li>
				<li class="b-menu-item how-choose">
					<a href="choose" class="b-menu-item__href">как<br>выбрать?</a>
				</li>
				<li class="b-menu-item useful-help">
					<a href="tips" class="b-menu-item__href">полезные советы</a>
				</li>
				<li class="b-menu-item user-feedback">
					<a href="feedback" class="b-menu-item__href">отзывы пользователей</a>
				</li>
				<li class="b-menu-item promo-actions">
					<a target="_blank" href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerlaunch" class="b-menu-item__href">Купить</a>
					<div class="b-menu-flags">
						<a href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive?utm_source=steamerlaunch&amp;utm_medium=sitelinks&amp;utm_campaign=steamerlaunch" target="_blank" class="b-menu__flag rus"></a>
						<a href="http://www.philips.ru/e/promo/promotions/general/byshops.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive" target="_blank" class="b-menu__flag kaz"></a>
						<a href="http://www.philips.ru/e/promo/promotions/general/kzshops.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive" target="_blank" class="b-menu__flag bel"></a>
					</div>
				</li>
			</ul>
			<span class="b-delimeter"></span>
		</div>
    <div class="b-content__over b-whatis-content">
        <!--<div class="b-white-delimeter"></div>-->
        <div class="b-tech-content__over">
            <h1 class="b-whatis__title b-tech__title">Технология Optimal Temp</h1>
	     <p class="b-tech-content__text b-tech-content-text__new">Встроенный микропроцессор поддерживает оптимальную температуру подошвы, а сверхмощный пар разглаживает складки на всех видах ткани. Джинсы и шелк можно гладить одновременно без риска прожечь шелк.</p>
            <img class="b-tech__img-top" src="images/6.png">
            <div class="b-tech-title-block">
                <p class="b-tech-title-block__text">Преимущества технологии<br>Optimal Temp</p>
            </div>
            <div class="b-tech-content__inner">
                <p class="b-tech-content__text">Вы часто гладите? В вашей корзине всегда много вещей из разных тканей? Устали сортировать белье перед глажкой? Сожгли как-то раз любимую вещь? Если ваш ответ утвердительный хотя бы на один из этих вопросов, то технология OptimalTemp – это то, что вам нужно.<br>И вот почему:</p>
                <div class="b-tech-point-block">
                    <div class="b-tech-point-block__digit digit-1"></div>
                    <p class="b-tech-point-block__text">Забудьте о переключении температуры для разных видов тканей: благодаря идеальному сочетанию пара и температуры у парогенератора есть только одна оптимальная настройка. Это стало возможным благодаря циклонической камере и специальному процессору Smart Control.</p>
                </div>
                <div class="b-tech-point-block">
                    <div class="b-tech-point-block__digit digit-2"></div>
                    <p class="b-tech-point-block__text">Это революция в технологии глажения. C парогенератором вы обеспечите 100% бережный уход для всех тканей, которые допускают глажение, даже таких, как кашемир или полиэстер. Можно гладить шелк сразу после джинсов, и сразу после этого – постельное белье или тюль. Сортировать вещи не нужно. Ни одна из вещей не пострадает и будет идеально отглажена!</p>
                </div>
                <div class="b-tech-point-block">
                    <div class="b-tech-point-block__digit digit-3"></div>
                    <p class="b-tech-point-block__text digit-3_text">Время, которое уходит на глажку – вот чего жаль даже больше, чем затраченных усилий. Все парогенераторы с технологией обладают высокой мощностью. Они готов к работе через 2 минуты с возможностью добавления воды в любое время. Теперь на глажку нужно в два раза меньше времени!</p>
                </div>
                <div class="b-tech-point-block">
                    <div class="b-tech-point-block__digit digit-4"></div>
                    <p class="b-tech-point-block__text digit-4_text">Объективные преимущества парогенераторов изучены, протестированы и одобрены независимыми экспертами по тканям – такими как Woolmark и I.W.T.O. Хотя, конечно, восхищенные отзывы наших покупателей мы тоже не сбрасываем со счетов!</p>
                </div>
                <div class="b-tech-point-block">
                    <div class="b-tech-point-block__digit digit-5"></div>
                    <p class="b-tech-point-block__text">Парогенераторы Philips обеспечивают высокое качество глажения. Больше не нужно проглаживать ткань раз за разом, добиваясь идеального результата. Великолепное качество разглаживания достигается благодаря постоянному воздействию пара под высоким давлением – до 6,5 бар. Он глубоко проникает в ткань и действует мгновенно.</p>
                </div>
                <div class="b-tech-arrow-block">
                    <a href="#" class="b-tech-arrow-block__arrow"></a>
                </div>
                
                <a name="innovations"></a>
                
                <h2 class="b-tech__evolution">Как рождаются инновации от Philips?</h2>
                <p class="b-tech__evolution_after">В этом интервью с инженером Philips Симоной Клуг вы узнаете, как происходит процесс разработки утюгов и парогенераторов Philips с технологией OptimalTemp. Разработчики Philips общаются с потребителями и совершенствуют процесс глажки, делая его эффективнее, удобнее и проще.</p>
                <div class="b-tech__video">
                    <iframe width="642" height="390" src="//www.youtube.com/embed/i8Y3UTrD5qg?wmode=transparent" frameborder="0" allowfullscreen></iframe>
                </div>
                <h2 class="b-tech__evolution">О технологии OptimalTemp</h2>
                <p class="b-tech__evolution_after">С каждым годом технологии все дальше уходят в будущее! А это значит, что к нашим услугам действительно продвинутые решения. Утюги и парогенераторы Philips с технологией OptimalTemp делают глажку простым и приятным занятием. Постоянная температура подошвы и мощный пар позволяют гладить даже шелк и джинсы на одном режиме. Без температурных настроек. Гладьте с удовольствием!</p>
                <div class="b-tech__video">
                    <iframe width="642" height="390" src="//www.youtube.com/embed/cRKTEUwsWoQ?wmode=transparent" frameborder="0" allowfullscreen></iframe>
                </div>
                <h2 class="b-tech__evolution">Самый компактный парогенератор</h2>
                <p class="b-tech__evolution_after">Последняя разработка Philips – компактный парогенератор с технологией OptimalTemp. Он не больше утюга, а гладит в 2 раза быстрее. А с помощью технологии OptimalTemp можно гладить любые ткани без настройки температуры. Экономьте время и место!</p>
                <div class="b-tech__video">
                    <iframe width="642" height="390" src="//www.youtube.com/embed/Dxg-Nxi4UNI?wmode=transparent" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="b-whatis-footer">
		<div class="b-whatis-footer__clouds"></div>
		<ul class="b-footer-menu">
            <li class="b-footer-item">
                <a href="index.html" class="b-footer-item__href">Пять причин</a>
            </li>
            <li class="b-footer-item">
                <a href="whatis.html" class="b-footer-item__href">Что такое парогенератор?</a>
            </li>
            <li class="b-footer-item">
                <a href="technology.html" class="b-footer-item__href active">Технология Optimal Temp</a>
            </li>
            <li class="b-footer-item b-footer-item__spacer_parent">
                <a href="choose" class="b-footer-item__href b-footer-item__spacer">Как выбрать?</a>
            </li>
            <li class="b-footer-item">
                <a href="tips" class="b-footer-item__href">Полезные советы</a>
            </li>
            <li class="b-footer-item">
                <a href="feedback.html" class="b-footer-item__href">Отзывы пользователей</a>
            </li>
            <li class="b-footer-item b-footer-item__last">
                <a target="_blank" href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerlaunch" class="b-footer-item__href">Промо акции</a>
            </li>
        </ul>
	</div>
</div>
<div class="popup_mail">
	<div class="popup">
    	<a target="_blank" href="http://www.shop.philips.ru/optimaltemp/technology/#innovations"><div class="hand1"></div></a>
        <div class="close">&#215;</div>
    </div><!-- popup -->
    <div class="girl"></div>
    <div class="hand2"></div>
</div><!-- popup_mail -->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/mousewheel.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/css3-mediaqueries.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.backgroundpos.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
</body>
</html>