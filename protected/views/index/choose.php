<!doctype html>
<html>
<head>

    <meta charset="utf-8">
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->

    <title>Как выбрать</title>

    <meta name="description" content="">
    <meta name="keywords" content="">

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/reset.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/fancybox/jquery.fancybox-1.3.4.css"  rel="stylesheet"/>
	
    <!--[if lte IE 9]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" rel="stylesheet" />
    <![endif]-->
    <!--[if lte IE 7]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie7.css" rel="stylesheet" />
    <![endif]-->
    <!--[if lte IE 8]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie8.css" rel="stylesheet" />
    <![endif]-->

	<script type="text/javascript">
	var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-3029220-4']);
	  _gaq.push(['_setCampSourceKey', 'origin']);
	  _gaq.push(['_trackPageview']);
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';


		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

</head>
<body>
<div class="b-body">
		<div class="b-menu">
			<ul class="b-menu-list">
				<li class="b-header__logo-block">
					<a target="_blank" href="http://www.philips.ru" class="b-header__logo"></a>
				</li>
				<li class="b-menu-item test-drive active">
					<a href="testdrive" class="b-menu-item__href b-menu-item__href_violet">акции</a>
				</li>
				<li class="b-menu-item five-reasons">
					<a href="index" class="b-menu-item__href">пять<br>причин</a>
				</li>
				<li class="b-menu-item generator">
					<a href="whatis" class="b-menu-item__href">что такое парогенератор?</a>
				</li>
				<li class="b-menu-item technology">
					<a href="technology" class="b-menu-item__href b-menu-item__href_violet">технология optimal temp</a>
				</li>
				<li class="b-menu-item how-choose">
					<a href="choose" class="b-menu-item__href">как<br>выбрать?</a>
				</li>
				<li class="b-menu-item useful-help">
					<a href="tips" class="b-menu-item__href">полезные советы</a>
				</li>
				<li class="b-menu-item user-feedback">
					<a href="feedback" class="b-menu-item__href">отзывы пользователей</a>
				</li>
				<li class="b-menu-item promo-actions">
					<a target="_blank" href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerlaunch" class="b-menu-item__href">Купить</a>
					<div class="b-menu-flags">
						<a href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive?utm_source=steamerlaunch&amp;utm_medium=sitelinks&amp;utm_campaign=steamerlaunch" target="_blank" class="b-menu__flag rus"></a>
						<a href="http://www.philips.ru/e/promo/promotions/general/byshops.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive" target="_blank" class="b-menu__flag kaz"></a>
						<a href="http://www.philips.ru/e/promo/promotions/general/kzshops.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive" target="_blank" class="b-menu__flag bel"></a>
					</div>
				</li>
			</ul>
			<span class="b-delimeter"></span>
		</div>
    <div class="b-content__over b-whatis-content">
        <div class="b-tips-content__over">
            <h1 class="b-whatis__title b-tips__title">Выбор парогенератора</h1>
            <div class="b-choose__question">Какая характеристика является для вас наиболее важной?</div>
            <ul class="b-choose-properties">
                <li class="b-choose__property property_small">
                    <a class="b-choose-property-selector select" data-cl="1">
                        <div class="b-choose-property__mask">
                            <img class="b-choose-property__img" src="images/7.png">
                        </div>
                        <div class="b-choose-property-selector-block">
                            <span class="b-choose-property__selector"></span>
                            <p class="b-choose-property__text">Компактность</p>
                        </div>
                    </a>
                </li>
                <li class="b-choose__property">
                    <a class="b-choose-property-selector" data-cl="2">
                        <div class="b-choose-property__mask">
                            <img class="b-choose-property__img" src="images/8.png">
                        </div>
                        <div class="b-choose-property-selector-block">
                            <span class="b-choose-property__selector"></span>
                            <p class="b-choose-property__text">Высокая мощность<br>и съемный резервуар</p>
                        </div>
                    </a>
                </li>
                <li class="b-choose__property">
                    <a class="b-choose-property-selector" data-cl="3">
                        <div class="b-choose-property__mask">
                            <img class="b-choose-property__img" src="images/9.png">
                        </div>
                        <div class="b-choose-property-selector-block">
                            <span class="b-choose-property__selector"></span>
                            <p class="b-choose-property__text">Большой объём глажки<br>без долива воды</p>
                        </div>
                    </a>
                </li>
                <li class="b-choose__property property_small">
                    <a class="b-choose-property-selector" data-cl="5">
                        <div class="b-choose-property__mask">
                            <img class="b-choose-property__img" src="images/10.png">
                        </div>
                        <div class="b-choose-property-selector-block">
                            <span class="b-choose-property__selector"></span>
                            <p class="b-choose-property__text">Тихая работа</p>
                        </div>
                    </a>
                </li>
            </ul>
            <div class="b-choose__extra-property">Дополнительные опции</div>
            <div class="b-choose-extra-sliders-block">
                <div class="b-choose-extra-carry-block">
                    <div class="b-choose-extra-slider">
                        <a href="#" class="b-choose-extra-slider-back">
                            <span class="b-choose-extra-slider__point"></span>
                        </a>
                    </div>
                    <p class="b-choose-extra__text">Carry-lock</p>
                    <p class="b-choose-extra__subtext">(фиксирует утюг для простоты<br>хранения и переноски)</p> 
                </div>
                <div class="b-choose-extra-off-block">
                    <div class="b-choose-extra-slider">
                        <a href="#" class="b-choose-extra-slider-back">
                            <span class="b-choose-extra-slider__point"></span>
                        </a>
                    </div>
                    <p class="b-choose-extra__text">Автовыключение</p>
                </div>
            </div>
        </div>
        <div class="b-choose-point-proposal-block__over">
            <div class="b-choose-point-proposal-block">
                <span class="b-choose-point__down"></span>
                <p class="b-choose-proposal__title">Парогенераторы, которые подойдут<br>именно вам</p>
                <div class="b-choose-proposal-over-block">
                    <a href="#" class="b-choose-proposal-over-block__left"></a>
                    <a href="#" class="b-choose-proposal-over-block__right"></a>
                    <div class="b-choose-proposal__over">
                        <ul class="b-choose-proposal-list">
                            <li class="b-choose-proposal-item it1">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC7619.png">
                                <p class="b-choose-proposal-item__text">Парогенератор Philips GC7619</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <!--<li class="b-choose-proposal-item__property">Паровой удар до 200г/мин</li>-->
                                    <li class="b-choose-proposal-item__property" style="margin-bottom:24px">Подача пара до 110г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-gc7619-25.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it1">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC7620.png">
                                <p class="b-choose-proposal-item__text">Парогенератор Philips GC7620</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 220г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-gc7619-1.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it1">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC7635.png">
                                <p class="b-choose-proposal-item__text">Парогенератор Philips GC7630</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 220г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-gc7630.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it1 it10">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC7635.png">
                                <p class="b-choose-proposal-item__text">Парогенератор Philips GC7635</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 240г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-gc7635.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it2">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC9222.png">
                                <p class="b-choose-proposal-item__text">Парогенератор<br>Philips PerfectCare GC9222</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 300г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-perfectcare-gc9222.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it2 it20">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC9231.png">
                                <p class="b-choose-proposal-item__text">Парогенератор<br>Philips PerfectCare GC9231</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 310г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href=" http://www.shop.philips.ru/parogenerator-philips-perfectcare-gc9231.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it2 it20 it200">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC9241.png">
                                <p class="b-choose-proposal-item__text">Парогенератор<br>Philips PerfectCare GC9241</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 340г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-perfectcare-gc9241.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it2 it20 it200">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC9246.png">
                                <p class="b-choose-proposal-item__text">Парогенератор<br>Philips PerfectCare GC9246</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 340г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-perfectcare-gc9246.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it3">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC8620.png">
                                <p class="b-choose-proposal-item__text">Парогенератор Philips GC8620</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 180г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-gc8620.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it3 it30">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC8630.png">
                                <p class="b-choose-proposal-item__text">Парогенератор Philips GC8630</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 220г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-gc8630-02.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
				<li class="b-choose-proposal-item it3 it30 it300">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC8635.png">
                                <p class="b-choose-proposal-item__text">Парогенератор Philips GC8635</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 220г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-gc8635.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it3 it30 it300">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC8640.png">
                                <p class="b-choose-proposal-item__text">Парогенератор Philips GC8640</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 220г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-gc8640-02.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it5">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC9545.png">
                                <p class="b-choose-proposal-item__text">Парогенератор<br>Philips PerfectCare GC9545</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 360г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-perfectcare-gc9545.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                            <li class="b-choose-proposal-item it5">
                                <img class="b-choose-proposal-item__img" src="images/Philips GC9550.png">
                                <p class="b-choose-proposal-item__text">Парогенератор<br>Philips PerfectCare GC9550</p>
                                <ul class="b-choose-proposal-item-properties">
                                    <li class="b-choose-proposal-item__property">Мощность 2400Вт</li>
                                    <li class="b-choose-proposal-item__property">Паровой удар до 360г/мин</li>
                                    <li class="b-choose-proposal-item__property">Подача пара до 120г/мин</li>
                                </ul>
                                <a target="_blank" href="http://www.shop.philips.ru/parogenerator-philips-perfectcare-gc9550.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal-item__href">Подробнее</a>
                            </li>
                        </ul>
			</div>
                </div>
                <a target="_blank" href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-choose-proposal__all">Посмотреть все модели</a>
            </div>
        </div>
    </div>
    <div class="b-whatis-footer">
        <div class="b-whatis-footer__clouds"></div>
        <ul class="b-footer-menu">
            <li class="b-footer-item">
                <a href="index" class="b-footer-item__href">Пять причин</a>
            </li>
            <li class="b-footer-item">
                <a href="whatis" class="b-footer-item__href">Что такое парогенератор?</a>
            </li>
            <li class="b-footer-item">
                <a href="technology" class="b-footer-item__href">Технология Optimal Temp</a>
            </li>
            <li class="b-footer-item b-footer-item__spacer_parent">
                <a href="choose" class="b-footer-item__href b-footer-item__spacer active">Как выбрать?</a>
            </li>
            <li class="b-footer-item">
                <a href="tips" class="b-footer-item__href">Полезные советы</a>
            </li>
            <li class="b-footer-item">
                <a href="feedback" class="b-footer-item__href">Отзывы пользователей</a>
            </li>
            <li class="b-footer-item b-footer-item__last">
                <a target="_blank" href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerchoice" class="b-footer-item__href">Промо акции</a>
            </li>
        </ul>
    </div>
</div>
<div class="popup_mail">
	<div class="popup">
    	<a target="_blank" href="http://www.shop.philips.ru/optimaltemp/technology/#innovations"><div class="hand1"></div></a>
        <div class="close">&#215;</div>
    </div><!-- popup -->
    <div class="girl"></div>
    <div class="hand2"></div>
</div><!-- popup_mail -->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/mousewheel.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/css3-mediaqueries.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.backgroundpos.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
</body>
</html>