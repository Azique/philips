<!doctype html>
<html>
<head>

    <meta charset="utf-8">
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->

    <title>Отзывы пользователей</title>

    <meta name="description" content="">
    <meta name="keywords" content="">

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/reset.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/fancybox/jquery.fancybox-1.3.4.css"  rel="stylesheet"/>
	
    <!--[if lte IE 9]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" rel="stylesheet" />
    <![endif]-->
    <!--[if lte IE 7]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie7.css" rel="stylesheet" />
    <![endif]-->
    <!--[if lte IE 8]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie8.css" rel="stylesheet" />
    <![endif]-->

	<script type="text/javascript">
	var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-3029220-4']);
	  _gaq.push(['_setCampSourceKey', 'origin']);
	  _gaq.push(['_trackPageview']);
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';


		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

</head>
<body>
<div class="b-body">
   <div class="b-clouds"></div>
		<div class="b-menu">
			<ul class="b-menu-list">
				<li class="b-header__logo-block">
					<a target="_blank" href="http://www.philips.ru" class="b-header__logo"></a>
				</li>
				<li class="b-menu-item test-drive active">
					<a href="testdrive" class="b-menu-item__href b-menu-item__href_violet">акции</a>
				</li>
				<li class="b-menu-item five-reasons">
					<a href="index" class="b-menu-item__href">пять<br>причин</a>
				</li>
				<li class="b-menu-item generator">
					<a href="whatis" class="b-menu-item__href">что такое парогенератор?</a>
				</li>
				<li class="b-menu-item technology">
					<a href="technology" class="b-menu-item__href b-menu-item__href_violet">технология optimal temp</a>
				</li>
				<li class="b-menu-item how-choose">
					<a href="choose" class="b-menu-item__href">как<br>выбрать?</a>
				</li>
				<li class="b-menu-item useful-help">
					<a href="tips" class="b-menu-item__href">полезные советы</a>
				</li>
				<li class="b-menu-item user-feedback">
					<a href="feedback" class="b-menu-item__href">отзывы пользователей</a>
				</li>
				<li class="b-menu-item promo-actions">
					<a target="_blank" href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerlaunch" class="b-menu-item__href">Купить</a>
					<div class="b-menu-flags">
						<a href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive?utm_source=steamerlaunch&amp;utm_medium=sitelinks&amp;utm_campaign=steamerlaunch" target="_blank" class="b-menu__flag rus"></a>
						<a href="http://www.philips.ru/e/promo/promotions/general/byshops.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive" target="_blank" class="b-menu__flag kaz"></a>
						<a href="http://www.philips.ru/e/promo/promotions/general/kzshops.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamertestdrive" target="_blank" class="b-menu__flag bel"></a>
					</div>
				</li>
			</ul>
			<span class="b-delimeter"></span>
		</div>
   <div class="b-content__over b-whatis-content">
        <!--<div class="b-white-delimeter"></div>-->
        <div class="b-whatis-content__over">
            <h1 class="b-whatis__title">Отзывы пользователей</h1>
            <div class="b-feedback__more">Больше отзывов читайте на <a href="http://dom.lady.mail.ru/testdrive.html" target="_blank" class="b-feedback__more_mail"></a></div>
            <ul class="b-feedback-list">
                <li class="b-feedback-item">
                    <div class="b-feedback-item-img">
                        <div class="b-feedback-item-img__mask"></div>
                        <img class="b-feedback-item__img" alt="" src="images/2.png">
                    </div>
                    <p class="b-feedback-item__title">Елена Пугачева</p>
                    <p class="b-feedback-item__text">Хочу рассказать, как происходило наше знакомство c Philips Perfect Care Aqua GC8620. Утюг легкий, по сравнению с тем агрегатом, которым я пользовалась, это нечто, подошва гладкая — утюг скользит по вещам, разглаживая все без усилий. Обтекаемая форма и удобная ручка, ну и хочется отметить резервуар для воды объемом 2,2 л. Начала я с шелка, результат отличный, ничего не испортилось и не сожглось. Полотенца, постельное белье, детские вещи, рубашки, джинсы — все прогладилось на «отлично», гораздо быстрее, чем я делала это раньше. А еще с парогенератором возможно вертикальное отпаривание, это очень удобно для некоторых вещей.</p>
                    <div class="b-feedback-item-full">
                        <a target="_blank" href="http://dom.lady.mail.ru/testdrivereview.html?id=4295" class="b-feedback-item__full-view b-feedback-item__full-view-text">Читать весь отзыв</a>
                        <!--<a href="#" class="b-feedback-item__full-view b-feedback-item__full-view-arrow"></a>-->
                    </div>
                </li>
                <li class="b-feedback-item">
                    <div class="b-feedback-item-img">
                        <div class="b-feedback-item-img__mask"></div>
                        <img class="b-feedback-item__img" alt="" src="images/3.png">
                    </div>
                    <p class="b-feedback-item__title">Анна Семенова</p>
                    <p class="b-feedback-item__text">Для меня все утюги абсолютно одинаковы. Все они символизируют глажку —  процесс утомительный и малоприятный. Но в один прекрасный день я испытала парогенератор Philips GC9222. Нагревается прибор очень быстро, буквально за пару минут. Начать я решила со своей любимой рубашки. <br>виселаююсолнышке, и с обычным утюгом тут бы пришлось покорпеть над каждой складкой, но парогенератор... Справился со всем меньше, чем за пять минут. Достаточно узкий носик утюга позволяет разгладить даже самые труднодоступные места между пуговиц и около манжет, а с помощью парового удара можно привести в порядок даже самые мятые участки ткани.</p>
                    <div class="b-feedback-item-full">
                        <a target="_blank" href="http://dom.lady.mail.ru/testdrive.html?id=4423" class="b-feedback-item__full-view b-feedback-item__full-view-text">Читать весь отзыв</a>
                        <!--<a href="#" class="b-feedback-item__full-view b-feedback-item__full-view-arrow"></a>-->
                    </div>
                </li>
                <li class="b-feedback-item">
                    <div class="b-feedback-item-img">
                        <div class="b-feedback-item-img__mask"></div>
                        <img class="b-feedback-item__img" alt="" src="images/4.png">
                    </div>
                    <p class="b-feedback-item__title">Екатерина Иванова</p>
                    <p class="b-feedback-item__text">Знакомство с парогенератором Philips PerfectCare Aqua GC8635 я решила начать с теории — для этого внимательно изучила инструкцию из коробки и на сайте Philips. Начать эксперимент я решила с самого, на мой взгляд, сложного — хлопчатобумажных и льняных салфеток. Их труднее всего прогладить осталось «мятых» следов, особенно линий сгиба. Подошва утюга очень гладкая и действительно волшебно скользит по ткани. В общем, десяток салфеток были поглажены практически за одно мгновение.</p>
                    <div class="b-feedback-item-full">
                        <a target="_blank" href="http://dom.lady.mail.ru/testdrive.html?id=3336" class="b-feedback-item__full-view b-feedback-item__full-view-text">Читать весь отзыв</a>
                        <!--<a href="#" class="b-feedback-item__full-view b-feedback-item__full-view-arrow"></a>-->
                    </div>
                </li>
                <li class="b-feedback-item">
                    <div class="b-feedback-item-img">
                        <div class="b-feedback-item-img__mask"></div>
                        <img class="b-feedback-item__img" alt="" src="images/5.png">
                    </div>
                    <p class="b-feedback-item__title">Ольга Попкова</p>
                    <p class="b-feedback-item__text">Я люблю гладить. Но с рождением дочери белья стало ощутимо больше, а вот времени гораздо меньше. Свое обычное количество белья при помощи парогенератора Philips PerfectCare GC9222 я глажу теперь вдвое быстрее. Причем глажу все сразу — постельное белье, рубашки, футболки, штаны, джинсы, одежду из деликатных тканей… Регулировка действительно не требуется и все прекрасно разглаживается. Немаловажный момент для мам: глажение при помощи пара – это дополнительная защита младенца, что особенно актуально в первые месяцы жизни.</p>
                    <div class="b-feedback-item-full">
                        <a target="_blank" href="http://dom.lady.mail.ru/testdrivereview.html?id=4931" class="b-feedback-item__full-view b-feedback-item__full-view-text">Читать весь отзыв</a>
                        <!--<a href="#" class="b-feedback-item__full-view b-feedback-item__full-view-arrow"></a>-->
                    </div>
                </li>
            </ul>
          <!--  <?php if (count($model)>0){ ?> -->
          <!--                 <ul class="b-feedback-list">
                    <?php foreach($model as $feedback){ ?>                    
                        <li class="b-feedback-item">
                            <div class="b-feedback-item-img">
                                <div class="b-feedback-item-img__mask"></div>
                                <img class="b-feedback-item__img" alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/<?php echo $feedback->img_fn; ?>">
                            </div>
                            <p class="b-feedback-item__title"><?php echo $feedback->fio; ?></p>
                            <p class="b-feedback-item__text"><?php echo $feedback->fulltext; ?></p>
                            <div class="b-feedback-item-full">
                                <?php if ($feedback->href != '') {?>
									<a target="_blank" href="<?php echo $feedback->href; ?>" class="b-feedback-item__full-view b-feedback-item__full-view-text">Читать весь отзыв</a>
								<?php } ?>
                                <!--<a href="#" class="b-feedback-item__full-view b-feedback-item__full-view-arrow"></a>-->
                            </div>
                        </li>
                    <?php } ?>
                </ul> -->
          <!--             <?php } ?> -->
			<div class="b-feedback__more down">Больше отзывов читайте на <a href="http://dom.lady.mail.ru/testdrive.html" target="_blank" class="b-feedback__more_mail"></a></div>
        </div>
    </div>
    <div class="b-whatis-footer">
		<div class="b-whatis-footer__clouds"></div>
		<ul class="b-footer-menu">
            <li class="b-footer-item">
                <a href="index" class="b-footer-item__href">Пять причин</a>
            </li>
            <li class="b-footer-item">
                <a href="whatis" class="b-footer-item__href">Что такое парогенератор?</a>
            </li>
            <li class="b-footer-item">
                <a href="technology" class="b-footer-item__href">Технология Optimal Temp</a>
            </li>
            <li class="b-footer-item b-footer-item__spacer_parent">
                <a href="choose" class="b-footer-item__href b-footer-item__spacer">Как выбрать?</a>
            </li>
            <li class="b-footer-item">
                <a href="tips" class="b-footer-item__href">Полезные советы</a>
            </li>
            <li class="b-footer-item">
                <a href="feedback" class="b-footer-item__href active">Отзывы пользователей</a>
            </li>
            <li class="b-footer-item b-footer-item__last">
                <a target="_blank" href="http://www.shop.philips.ru/appliances/irons/steam-ironing.html?utm_source=steamerlaunch&utm_medium=sitelinks&utm_campaign=steamerlaunch" class="b-footer-item__href">Промо акции</a>
            </li>
        </ul>
	</div>
</div>
<div class="popup_mail">
	<div class="popup">
    	<a target="_blank" href="http://www.shop.philips.ru/optimaltemp/technology/#innovations"><div class="hand1"></div></a>
        <div class="close">&#215;</div>
    </div><!-- popup -->
    <div class="girl"></div>
    <div class="hand2"></div>
</div><!-- popup_mail -->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/mousewheel.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/css3-mediaqueries.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.backgroundpos.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
</body>
</html>