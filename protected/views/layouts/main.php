<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/old_main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/promouser.css" />

	<title>KBCreative</title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->
	<div id="mainmenu">
		<?php 
            /*$items[] = '';
            $statPush = false;
            $last = false;
            $statItem = 3; // Порядковый номер элемента. Счетчик начинается с 1(потому что 0 - главная, и она всегда первая).
            $counter = 1;
            $menu = $this->getMenu();
            $len = count($menu);
            array_push($items, array('label'=>'Главная', 'url'=>$this->createAbsoluteUrl('/')));
            foreach ($menu as $m){
                if (($counter >= $statItem and !$statPush) or ($counter == $len and !$statPush)){
                    if ($counter == $len and $counter != $statItem){
                        array_push($items, array('label'=>$m->attributes['title'], 'url'=>$m->attributes['url']));
                        $last = true;
                    }
                    array_push($items, array('label'=>'Каталог', 'url'=>array('/catalog/')));
                    $statPush = true;
                }
                if (!$last){
                    array_push($items, array('label'=>$m->attributes['title'], 'url'=>$m->attributes['url']));
                    $counter++;
                }
            }*/
        
            $this->widget('zii.widgets.CMenu',array(
    			'items'=>array(
                    array('label'=>'Потребительские товары', 'url'=>$this->createAbsoluteUrl('/')),
    				//array('label'=>'Страница', 'url'=>array('/page/test')),
                    //array('label'=>'Пример отсутствующей страницы', 'url'=>array('/page/test1234')),
    				array('label'=>'Профессиональные решения', 'url'=>$this->createAbsoluteUrl('/')),
                    //array('label'=>'Пример отсутвующего элемента', 'url'=>array('/catalog/test/fifth')),
                    array('label'=>'О компании', 'url'=>$this->createAbsoluteUrl('/')),
                    //array('label'=>'Войти', 'url'=>array('/user/login'), 'visible'=>Yii::app()->user->isGuest, 'linkOptions'=>array('class'=>'in')),
                    //array('label'=>'Регистрация', 'url'=>array('/user/registration'), 'visible'=>Yii::app()->user->isGuest, 'linkOptions'=>array('class'=>'reg')),
                    //array('label'=>'Личный кабинет', 'url'=>array('/user/editprofile'), 'visible'=>!Yii::app()->user->isGuest),
                    //array('label'=>'Выйти ('.Yii::app()->user->name.')', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest),
    				//array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
    				//array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
                   
    			), 
    		)); 
        ?>
	</div><!-- mainmenu -->
    
	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		KBCreative. 2013
	</div><!-- footer -->

</div><!-- page -->
<?php 
if (Yii::app()->params['ajax-users']){
?>
<div class="b-modal"></div>
<div class="b-popup login">
    <?php 
        $this->setLoginForm();
        $login_model = $this->getLoginForm();
        $form=$this->beginWidget('CActiveForm', array(
        	'id'=>'login-form',
        )); 
    ?>
    
    <div class="row">
    	<?php echo $form->labelEx($login_model,'username', array('class'=>'ajax-label')); ?>
    	<?php echo $form->textField($login_model,'username'); ?>
    	<?php echo $form->error($login_model,'username', array('class'=>'ajax-label')); ?>
    </div>
    
    <div class="row">
    	<?php echo $form->labelEx($login_model,'password', array('class'=>'ajax-label')); ?>
    	<?php echo $form->passwordField($login_model,'password'); ?>
    	<?php echo $form->error($login_model,'password', array('class'=>'ajax-label')); ?>
    </div>
    
    <div class="row rememberMe">
    	<?php echo $form->checkBox($login_model,'rememberMe'); ?>
    	<?php echo $form->label($login_model,'rememberMe'); ?>
    	<?php echo $form->error($login_model,'rememberMe', array('class'=>'ajax-label')); ?>
    </div>
        
    <div class="row restorePass">
        <?php echo CHtml::link('Восстановить пароль', array('#restore'), array('class'=>'restore_pass')); ?>
    </div>
    
    <div class="row buttons">
    	<?php echo CHtml::submitButton('Войти', array('class'=>'ajax-login')); ?>
    </div>
    
    <?php $this->endWidget(); ?>
</div>
<div class="b-popup restore">
<?php
    echo CHtml::form(array('/'), 'post',array('id'=>'restore-form'));
    echo CHtml::label('Введите ваш email:', 'email', array('class'=>'ajax-label'));
    echo CHtml::textField('email');
    echo CHtml::submitButton('Отправить запрос', array('class'=>'ajax-restore'));
    echo CHtml::endForm();
?>
</div>
<div class="b-popup registration">
    <?php 
    $this->setRegForm();
    $reg_model = $this->getRegForm();
    $form=$this->beginWidget('CActiveForm', array(
    	'id'=>'promousers-form',
    )); ?>

	<p class="note">Поля, отмеченные <span class="required">*</span>, обязательны для заполнения.</p>

	<?php echo $form->errorSummary($reg_model); ?>

	<div class="row">
		<?php echo $form->labelEx($reg_model,'second_name', array('class'=>'ajax-label')); ?>
		<?php echo $form->textField($reg_model,'second_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($reg_model,'second_name', array('class'=>'ajax-label')); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($reg_model,'first_name', array('class'=>'ajax-label')); ?>
		<?php echo $form->textField($reg_model,'first_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($reg_model,'first_name', array('class'=>'ajax-label')); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($reg_model,'last_name', array('class'=>'ajax-label')); ?>
		<?php echo $form->textField($reg_model,'last_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($reg_model,'last_name', array('class'=>'ajax-label')); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($reg_model,'password', array('class'=>'ajax-label')); ?>
		<?php echo $form->passwordField($reg_model,'password',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($reg_model,'password', array('class'=>'ajax-label')); ?>
	</div>

    <div class="row">
  		<?php echo $form->labelEx($reg_model,'password_repeat', array('class'=>'ajax-label')); ?>
  		<?php echo $form->passwordField($reg_model,'password_repeat',array('size'=>60,'maxlength'=>255)); ?>
  		<?php echo $form->error($reg_model,'password', array('class'=>'ajax-label')); ?>
   	</div>
    
	<div class="row">
		<?php echo $form->labelEx($reg_model,'email', array('class'=>'ajax-label')); ?>
		<?php echo $form->textField($reg_model,'email',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($reg_model,'email', array('class'=>'ajax-label')); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($reg_model,'phone', array('class'=>'ajax-label')); ?>
		<?php echo $form->textField($reg_model,'phone',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($reg_model,'phone', array('class'=>'ajax-label')); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($reg_model,'address', array('class'=>'ajax-label')); ?>
		<?php echo $form->textField($reg_model,'address',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($reg_model,'address', array('class'=>'ajax-label')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Создать', array('class'=>'ajax-reg')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
<?php
    echo '<script type="text/javascript" src="'.Yii::app()->request->baseUrl.'/js/ext/jquery-1.8.3.min.js"></script>';
    echo '<script type="text/javascript" src="'.Yii::app()->request->baseUrl.'/js/promouser.js"></script>';
}
else{
    echo '<script type="text/javascript" src="'.Yii::app()->request->baseUrl.'/js/ext/jquery-1.8.3.min.js"></script>';
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
</body>
</html>
