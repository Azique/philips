<?php
/**
 * @copyright Copyright (c) 2009-2011 Rodolfo Gonzalez <metayii.framework@gmail.com>
 *
 * @version 2.0
 * @license LGPL 2.1
 *
 * {@link http://www.gnu.org/licenses/lgpl-2.1.txt}
 */

/**
 * ETinyMce is an input widget based on TinyMCE and the jQuery TinyMCE plugin.
 *
 * Example:
 *
 * <code>
 * <?php $this->widget('application.extensions.tinymce.ETinyMce',
 * array('name'=>'html')); ?>
 * </code>
 *
 * This extension includes TinyMCE 3.4.3.2 Jquery, Compressor 2.0.4 PHP and the
 * full language sets. This is included under the LGPL 2.1 license:
 *
 * @license http://tinymce.moxiecode.com/js/tinymce/jscripts/tiny_mce/license.txt
 *
 * @author Rodolfo Gonzalez <metayii.framework@gmail.com>
 */
class ckeditor extends CInputWidget {

    public $options = array();

    public function run() {
        list($name, $id) = $this->resolveNameID();

        // Publishing assets.
        $assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
        
        // Registering javascript.
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        
        $cs->registerScriptFile($assets . '/ckeditor.js');

        // Подключаем файл настроек редактора, если он указан и существует
        if(isset($this->options['editor'])) {
            if (file_exists(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets' . '/config.'.$this->options['editor'].'.js')) {
                $cs->registerScriptFile($assets . '/config.'.$this->options['editor'].'.js');
            }
        }
        /*$cs->registerScript(
            'Yii.' . get_class($this) . '#' . $id, '$(function(){CKEDITOR.replace(' . CJavaScript::encode($this->options) . ');});'
        );*/
        
        $cs->registerScript(
            'Yii.' . get_class($this) . '#' . $id, '$(function(){CKEDITOR.replace('.$id.', {
            	filebrowserBrowseUrl : "/ckfinder/ckfinder.html",
            	filebrowserImageBrowseUrl : "/ckfinder/ckfinder.html?type=Images",
            	filebrowserFlashBrowseUrl : "/ckfinder/ckfinder.html?type=Flash",
            	filebrowserUploadUrl : "/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files",
            	filebrowserImageUploadUrl : "/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images",
            	filebrowserFlashUploadUrl : "/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"
            });});'
        );                

        // подключаем css стили если они указаны в опциях * пока жёстко *
        $css = '';
        $css .= (isset($this->options['height']) and $this->options['height']) ? 'height:'.$this->options['height'].';' : '';
        $css .= (isset($this->options['width']) and $this->options['width']) ? 'width:'.$this->options['width'].';' : '';
        if ($css !== '') $this->htmlOptions['style'] = $css;

        if ($this->hasModel()) {
            $html = CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
        } else {
            $html = CHtml::textArea($name, $this->value, $this->htmlOptions);
        }
        echo $html;
    }
}