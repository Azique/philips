<?php

/**
 * This is the model class for table "catalog".
 *
 * The followings are the available columns in table 'catalog':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $text
 * @property string $url
 */
class Feedback extends CActiveRecord
{
    
    public $img_fn;
    public $maxPriority;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'feedback';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fio, fulltext', 'required'),
			array('img_fn, fio, href', 'length', 'max'=>255),
            array('img_fn', 'file', 'types'=>'jpg, gif, png', 'maxSize' => 5242880, 'allowEmpty'=>true, 'safe'=>true),
			array('fio, fulltext, href, img_fn', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fio' => 'ФИО',
			'fulltext' => 'Полный текст',
            'img_fn' => 'Изображение отзыва',
            'priority' => 'Приоритет',
			'href' => 'Ссылка'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('fulltext',$this->fulltext,true);
		$criteria->compare('img_fn',$this->img_fn,true);
		$criteria->compare('href',$this->href,true);
        $criteria->compare('priority',$this->priority,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=> array(
                'defaultOrder' => 'priority ASC',
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Catalog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public function beforeSave(){
        
        $file=CUploadedFile::getInstance($this,'img_fn');
        if (!$this->isNewRecord and !empty($file)){
            $oldmodel = Feedback::model()->find('id = :id', array(':id' => $this->id));
            if ($oldmodel->img_fn != ''){
                unlink(dirname(Yii::app()->request->scriptFile).DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$oldmodel->img_fn);
            }
        }
        if (!empty($file)){
            $extension = strtolower($file->extensionName);
            $filename = FileHelper::getRandomFileName((Yii::app()->request->scriptFile).DIRECTORY_SEPARATOR.'images', $extension);
            $this->img_fn = $filename . '.' . $extension;
            $url = dirname(Yii::app()->request->scriptFile).DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->img_fn;
            $file->saveAs($url);
        }
        else{
            if (!$this->isNewRecord){
                $oldmodel = Feedback::model()->find('id = :id', array(':id' => $this->id));
                $this->img_fn = $oldmodel->img_fn;
            }
        }
        //$image = Yii::app()->image->load($url);
        //$image->resize(100, 100);
        //$image->save(dirname(Yii::app()->request->scriptFile).DIRECTORY_SEPARATOR.'checks'.DIRECTORY_SEPARATOR.'thumbs'.DIRECTORY_SEPARATOR.$this->check_fn);
        /*if (!$this->isNewRecord){
            $this->priority = 100;
        }*/
		
		if ($this->isNewRecord){
			$criteria = new CDbCriteria;
			$criteria->select= 'max(priority) AS maxPriority';
			$row = Feedback::model()->find($criteria);
			$this->priority = $row['maxPriority'] + 100;
		}

        return parent::beforeSave();
    }
}
