<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	private $_pageTitle;
    private $_pageKeywords;
    private $_pageDescription;
	private $_menuVis;
    private $_menu;
    private $_loginForm;
    private $_regForm;
    
	/**
	 * @return CAction the action currently being executed, null if no active action.
	 */
	public function getMenuVis()
	{
		if ($this->_menuVis == null){
			return '';
		}
		else{
			return $this->_menuVis;
		}
	}

	/**
	 * @param CAction $value the action currently being executed.
	 */
	public function setMenuVis($value)
	{
		$this->_menuVis=$value;
	}

    /**
	 * @return CAction the action currently being executed, null if no active action.
	 */
	public function getMenu()
	{
		return Page::model()->findAll('visible = 1');
	}

    /**
	 * @return string the page keywords. Defaults to the controller name and the action name.
	 */
	public function getKeywords()
	{
		if($this->_pageKeywords!==null)
			return $this->_pageKeywords;
		else
		{
			return '';
		}
	}

	/**
	 * @param string $value the page keywords.
	 */
	public function setKeywords($value)
	{
		$this->_pageKeywords=$value;
	}

    /**
	 * @return string the page keywords. Defaults to the controller name and the action name.
	 */
	public function getDescription()
	{
		if($this->_pageDescription!==null)
			return $this->_pageDescription;
		else
		{
			return '';
		}
	}

	/**
	 * @param string $value the page keywords.
	 */
	public function setDescription($value)
	{
		$this->_pageDescription=$value;
	}
    
    public function setLoginForm(){
        $this->_loginForm = new LoginForm;
    }
    
    public function getLoginForm(){
        return $this->_loginForm;
    }
    
    public function setRegForm(){
        $this->_regForm = new Promousers;
    }
    
    public function getRegForm(){
        return $this->_regForm;
    }
}