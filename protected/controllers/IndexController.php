<?php

class IndexController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    
	/**
	 * index page
	 */
	public function actionIndex($lang = ''){
		$this->renderPartial('index');
	}
    
    /**
	 * error page
	 */
    public function actionError($lang = ''){
		$this->renderPartial('error');
	}
    
    /**
     * whatis page
     */
    public function actionWhatis($lang = ''){
        $this->renderPartial('whatis');
    }
    
	/**
     * technology page
     */
    public function actionTechnology($lang = ''){
        $this->renderPartial('technology');
    }
	
	/**
     * choose page
     */
    public function actionChoose($lang = ''){
        $this->renderPartial('choose');
    }
	
	/**
	 * tips page
	 */
	public function actionTips($lang = ''){
		$this->renderPartial('tips');
	}
	
	/**
	 * tips page
	 */
	public function actionTestdrive($lang = ''){
		$this->renderPartial('testdrive');
	}
	
    /**
     * feedback page
     */
    public function actionFeedback($lang = ''){
		$this->renderPartial('feedback');
       // $model = Feedback::model()->findAll();
        //$this->renderPartial('feedback', array('model'=>$model));
    }
}
