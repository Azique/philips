﻿(function(){

	;(function(){ // функция работы страницы советов
		var tips = ({

			curState:1, 

			newState:0,

			rollInMove:false,

			init: function(){
				if($('.b-tips-videos-block').length){
					this.bind();
				}
				return;
			},

			bind: function(){
				var that = this;
				$('.b-tips-videos-block__prev').click(function(){
					if (!that.rollInMove){
						$('.b-tips-videos-pagionation').animate({top: '+='+111}, 250, function(){
							$('.b-tips-videos-block__prev').hide();
							$('.b-tips-videos-block__next').show();
							that.rollInMove = true;
							setTimeout(function(){
								that.rollInMove = false;
							});
						});
					}
					return false;
				});
				$('.b-tips-videos-block__next').click(function(){
					if (!that.rollInMove){
						$('.b-tips-videos-pagionation').animate({top: '-='+111}, 250, function(){
							$('.b-tips-videos-block__prev').show();
							$('.b-tips-videos-block__next').hide();
							that.rollInMove = true;
							setTimeout(function(){
								that.rollInMove = false;
							});
						});
					}
					return false;
				});
				$('.b-tips-videos-pagination__item-href').click(function(){
					that.newState = parseInt($(this).attr('data-href'));
					var title = $(this).children('.b-tips-title-on-change').html();
					var text = $(this).children('.b-tips-text-on-change').html();
					var share_text = $(this).children('.b-tips-text-share-on-change').html();
					if (that.curState !== that.newState){
						$('.video' + that.curState).fadeOut();
						$('.video' + that.newState).stop(true, true).fadeIn();
						$('.b-tips-change__title').html(title);
						$('.b-tips-change__text').html(text);
						$('.b-tips-change__text-hide').html(share_text);
						that.curState = that.newState;
					}
					return false;
				});
				$('.b-tips-share__vk').click(function(){	
					var url = $('.video'+that.curState).attr('data-href');
					Share.vkontakte("http://www.shop.philips.ru/optimaltemp", $('.b-tips-change__title').text(), 'http://'+window.location.hostname+'/optimaltemp'+'/i/Philips_Landing6_100.jpg',$('.b-tips-change__text').text().substr(0, 472));
					return false;
				});
				$('.b-tips-share__fb').click(function(){
					var url = $('.video'+that.curState).attr('data-href');
					Share.facebook("http://www.shop.philips.ru/optimaltemp", $('.b-tips-change__title').text(), 'http://'+window.location.hostname+'/optimaltemp'+'/i/Philips_Landing6_100.jpg',$('.b-tips-change__text-hide').text());
					return false;
				});
			}

		}).init();
	})();

	;(function(){ // функция работы страница выбора
		var choose = ({
			
			curState:1,

			newState:0,

			chooseInMove:false,

			init: function(){
				if($('.b-choose-extra-sliders-block').length){
					this.bind();
				}
				return;
			},

			bind: function(){
				var that = this;
				$('.b-choose-property-selector').click(function(){
					$('.b-choose-property-selector.select').removeClass('select');
					$(this).addClass('select');
					that.changeChoose();
					return false;
				});
				$('.b-choose-extra-slider').click(function(){
					$(this).toggleClass('active');
					that.changeChoose();
					return false;
				});
				$('.b-choose-proposal-over-block__right').click(function(){
					var that = this;
					//var leftC = parseInt($('.b-choose-proposal-list').css('left'));
					if (!that.chooseInMove){
						$('.b-choose-proposal-list').animate({left: '-=' + 250}, 250, function(){ 
							if (parseInt($('.b-choose-proposal-list').css('left')) === -2250){
								$('.b-choose-proposal-list').css('left', '-1000px');
							} 
						});
						that.chooseInMove = true;
						setTimeout(function(){
							that.chooseInMove = false;
						}, 250);
					}
					return false;
				});
				$('.b-choose-proposal-over-block__left').click(function(){
					var that = this;
					//var left = parseInt($('.b-choose-proposal-list').css('left'));
					if (!that.chooseInMove){
						$('.b-choose-proposal-list').animate({left: '+=' + 250}, 250, function(){
							if (parseInt($('.b-choose-proposal-list').css('left')) === 0){
								$('.b-choose-proposal-list').css('left', '-1250px');
							}
						});
						that.chooseInMove = true;
						setTimeout(function(){
							that.chooseInMove = false;
						}, 250);
					}
					return false;
				});
			},

			changeChoose: function(){
				var ch = parseInt($('.b-choose-property-selector.select').attr('data-cl'));
				var cl = 0;
				var ao = 0;
				if ($('.b-choose-extra-carry-block').children('.active').length != 0){
					cl = 1;
				}
				if ($('.b-choose-extra-off-block').children('.active').length != 0){
					ao = 1;
				}
				
				if (ch === 1){
					if (ao === 0 && cl === 0){
						this.newState = 1;	
					}
					else{
						this.newState = 10;
					}
				}
				
				if (ch === 2){
					if (ao === 0 && cl === 0){
						this.newState = 2;
					}
					else{
						if (ao === 0 && cl === 1){
							this.newState = 20;
						}
						else{
							this.newState = 200;
						}
					}
				}
				
				if (ch === 3){
					if (ao === 0 && cl === 0){
						this.newState = 3;
					}
					else{
						if (ao === 0 && cl === 1){
							this.newState = 30;
						}
						else{
							this.newState = 300;
						}
					}
				}
				
				if (ch === 5){
					this.newState = 5;
				}

				if (this.newState !== this.curState){
					this.showNewChoose();
				}
				$('html:not(:animated),body:not(:animated)').animate({scrollTop: $('.b-choose-properties').offset().top - 64 }, 500);
				return;
			},

			showNewChoose: function(){
				$('.it'+this.curState).stop(true, true).fadeOut(10);
				$('.it'+this.newState).stop(true, true).fadeIn();
				if ( !jQuery.support.inlineBlockNeedsLayout ) {
				    $('.it'+this.newState).css("display", "inline-block");
				} else {
				    $('.it'+this.newState).css("display", "inline");
				    $('.it'+this.newState).css("zoom", "1");
				}
				/*if (this.newState === 2){
					//$('.b-choose-proposal-list').css('width', '3250px');
					//$('.b-choose-proposal-list').css('left', '-1000px');
					//this.showArrows();
				}
				else{
					$('.b-choose-proposal-list').css('width', 'auto');
					$('.b-choose-proposal-list').css('left', '0');
					this.hideArrows();
				}*/
				this.curState = this.newState;
				return;
			},

			showArrows: function(){
				$('.b-choose-proposal-over-block__left').show();
				$('.b-choose-proposal-over-block__right').show();
				return;
			},

			hideArrows: function(){
				$('.b-choose-proposal-over-block__left').hide();
				$('.b-choose-proposal-over-block__right').hide();
				return;
			}

		}).init();
	})();

	;(function(){
		// функция работы пагинции

		var pagination = ({

			currentPage:0,

			animationInMove:false, 

			maxPage:5,

			time:300,

			direction : "down",

			top : true,

			init: function(){
				if ($('.b-pagination').length){
					this.bind();
				}
				return;
			},

			bind: function(){
				var _this = this;
				$(window).scroll(function(){
					var yPos = -($(window).scrollTop());
					var coords = 'center ' + (yPos / 2) + 'px';
					var coordsSmall = 'center ' + (yPos * 2) + 'px';
					$('.b-body').queue("fx", []).animate({backgroundPosition:  coords}, _this.time, "easeOutQuad");
					$('.b-clouds').queue("fx", []).animate({backgroundPosition:  coordsSmall}, _this.time, "easeOutQuad");
					if((-yPos + 185) < $('.temp').eq(0).offset().top){
						$('.b-pagination__elem.active').removeClass('active');
					}
					else{
						for (var i = 0; i < $('.temp').length; i++){
							if ((-yPos + 185) > $('.temp').eq(i).offset().top && !$('.b-pagination__elem').eq(i).hasClass('active')){
								if ((i+1) === 5 || (-yPos+100) < $('.temp').eq(i+1).offset().top){
									$('.b-pagination__elem.active').removeClass('active');
									$('.b-pagination__elem').eq(i).addClass('active');
								}
							}
						}
					}
					if ($(window).scrollTop() >= ($(document).height() - $(window).height())){
						$('.b-pagination__elem.active').removeClass('active');
						$('.b-pagination__elem').eq(4).addClass('active');
					}
				});

				$('.b-five-steps__arrow, .b-step1__arrow, .b-step2__arrow, .b-step3__arrow, .b-step4__arrow').bind('click', function(){
					var p = parseInt($(this).attr('href'));
					$('.b-pagination__elem').eq(p-1).click();
					return false;
				});

				$(".fancy-video").click(function() {
					$.fancybox({
							'padding'		: 0,
							'autoScale'		: false,
							'transitionIn'	: 'none',
							'transitionOut'	: 'none',
							'title'			: this.title,
							'width'			: 680,
							'height'		: 495,
							'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
							'type'			: 'swf',
							'swf'			: {
							   	 'wmode'		: 'transparent',
								'allowfullscreen'	: 'true'
							}
						});
					return false;
				});

				$('.b-pagination__elem').bind('click', function(){
					_this.top = false;
					var bet = 0;
					if (!_this.animationInMove && !$(_this).hasClass('active')){
						if (prevhref === $('.b-pagination__elem.active').attr('href')){
							var prevhref = 0;
						}
						else{
							var prevhref = $('.b-pagination__elem.active').attr('href').substr(1);
						}
						var href = $(this).attr('href').substr(1);
						_this.currentPage = href;
						$('.b-pagination__elem.active').removeClass('active');
						$('.b-pagination__elem').eq(_this.currentPage-1).addClass('active');
						if (_this.currentPage === "1"){bet = 100};
						if (_this.currentPage === "2"){bet = 130};
						if (_this.currentPage === "3"){bet = 115};
						if (_this.currentPage === "4"){bet = 80};
						if (_this.currentPage === "5"){bet = 120};
						$('html:not(:animated),body:not(:animated)').animate({scrollTop: $('.b-steps-'+(_this.currentPage)).prev().offset().top - bet }, this.time);

						if (href > prevhref){
							var delta = -400 * (href-prevhref);
						}
						else{
							var delta = 400 * (prevhref-href);
						}
						if (($.browser.msie) && (($.browser.version == '6.0') || ($.browser.version == '7.0') || ($.browser.version == '8.0'))) {
							var yPos = parseInt($('.b-body').css('background-position-y'));
						}
						else{
							var backgroundPos = $('.b-body').css('backgroundPosition').split(" ");
							var yPos = parseInt(backgroundPos[1]);
						}
						$('.b-body').animate({backgroundPosition:  "0px " + (yPos + delta) + 'px'},  this.time + 100);
					}
					return false;
				});
				/*$('html').keydown(function(e){
					var success = false;
					if (!_this.animationInMove){
						_this.animationInMove = true;
						if (e.which === 40){
							_this.direction = "down";
							if (_this.currentPage < _this.maxPage){
								_this.currentPage++;
								_this.top = false;
								success = true;
							}
							else{
								_this.restoreTime();
								return true;
							}	
						}
						if (e.which === 38){
							_this.direction = "up";
							if (_this.currentPage > 1){
								_this.currentPage--;
								_this.top = false;
								success = true;
							}
							else{
								if (!_this.top){
									_this.top = true;
									_this.goTop();
								}
								else{
									_this.restoreTime();
									return true;
								}
							}
						}
						if (success){
							_this.changePager();
						}
						_this.restoreTime();
					}
					else{
						return false;
					}
					return;
				});*/
			},

			changePager: function(){
				var anim = this.backMove();
				$('.b-pagination__elem.active').removeClass('active');
				$('.b-pagination__elem').eq(this.currentPage-1).addClass('active');
				$('html:not(:animated),body:not(:animated)').animate({scrollTop: $('.b-steps-'+(this.currentPage)).prev().offset().top -140 }, this.time);
				$('.b-body').animate({backgroundPosition:  "0px " + anim}, this.time + 100);
				return;
			},

			goTop: function(){
				var anim = this.backMove();
				$('.b-pagination__elem.active').removeClass('active');
				$('html:not(:animated),body:not(:animated)').animate({scrollTop: 0}, this.time);
				this.currentPage = 0;
				$('.b-body').animate({backgroundPosition:  "0px " +  anim}, this.time + 100);
				return;
			},

			backMove: function(){
				if (this.direction === "down"){
					var delta = -400;
				}
				else{
					var delta = 400;
				}
				if (($.browser.msie) && (($.browser.version == '6.0') || ($.browser.version == '7.0') || ($.browser.version == '8.0'))) {
					var yPos = parseInt($('.b-body').css('background-position-y'));
				}
				else{
					var backgroundPos = $('.b-body').css('backgroundPosition').split(" ");
					var yPos = parseInt(backgroundPos[1]);
				}
				return yPos + delta + 'px';
			},

			restoreTime: function(){
				var _this = this;
				setTimeout(function(){
					_this.animationInMove = false;
				}, this.time + 100);
				return;
			}

		}).init();
	})();

	;(function(){ // js на странице "что такое парогенератор"

		var how = ({

			time:300,

			init: function(){
				if($('.b-whatis-key').length){
					this.bind();
				}
				return;
			},

			bind: function(){
				var _this = this;
				/*$('.b-whatis-block__key-href, .b-whatis-block__key-loc-href').hover(
					function(){
						var cl = $(this).attr('class').split(' ');
						cl = cl[1];
						$('.'+cl).addClass('hover');
					},
					function(){
						var cl = $(this).attr('class').split(' ');
						cl = cl[1];
						$('.'+cl).removeClass('hover');
					}
				);*/
				$('html').click(function(){
					if ($('.b-whatis-iron-helper.active').length){
						$('.b-whatis-iron-helper.active').removeClass('active');
					}
				});
				$('.b-whatis-block__key-href, .b-whatis-block__key-loc-href').click(function(){
					var cl = $(this).attr('class').split(' ');
					var href = $(this).attr('href').substr(1);
					cl = cl[1];
					_this.clearActivePoint();
					$('.'+cl).addClass('active');
					$('[data-key='+ href +']').removeClass('hide');
					return false;
				});
				$('.b-whatis-key__href').click(function(){
					if (!$(this).hasClass('active')){
						var href = $(this).attr('href').substr(1);
						_this.clearActive();
						$('[data-key='+ href +']').removeClass('hide');
						if (href === 'key'){
							$('.b-whatis-key__back').queue("fx", []).animate({'left':'6px'}, _this.time);
						}
						else{
							$('.b-whatis-key__back').queue("fx", []).animate({'left':'203px'}, _this.time);
						}
						$('.b-whatis-key__href.active').removeClass('active');
						$('.'+href).addClass('active');
						if ($('.b-whatis-iron-helper.active').length){
							$('.b-whatis-iron-helper.active').removeClass('active');
						}
					}
					return false;
				});
				$('.b-whatis-iron-helper').click(function(){
					if ($(this).hasClass('active')){
						$(this).removeClass('active');
					}
					else{
						if ($('.b-whatis-iron-helper.active').length){
							$('.b-whatis-iron-helper.active').removeClass('active');
						}
						$(this).addClass('active');
					}
					return false;
				});
				return;
			},

			clearActive: function(){
				$('.b-whatis-block').addClass('hide');
				$('.b-whatis-text-block__text').addClass('hide');
				//$('.b-whatis-arrow-block').addClass('hide');
				return;
			},

			clearActivePoint: function(){
				$('.qs, .ps, .at, .lr, .st, .ot').removeClass('active');
				$('.b-whatis-block-text').addClass('hide');
				return;
			}



		}).init();

	})();


	;(function(){ // анимация стрелочки в тизере
			//.b-tech-arrow-block__arrow, .b-step1__arrow, .b-step2__arrow, .b-step3__arrow, .b-step4__arrow
          var el = $('.b-five-steps__arrow'), el1 = $('.b-tech-arrow-block__arrow'), el2 = $('.b-step1__arrow'), el3 = $('.b-step2__arrow'), el4 = $('.b-step3__arrow'), el5 = $('.b-step4__arrow'),
              interval = 500,
			  bool = false;

        setInterval(function(){
            el.animate({
                bottom: '+=' + 5
            }, 500, function(){
                el.animate({
                    bottom: '-=' + 5
                }, 500)
            })
        }, interval*2 + 100);

	setInterval(function(){
            el1.animate({
                bottom: '+=' + 5
            }, 500, function(){
                el1.animate({
                    bottom: '-=' + 5
                }, 500)
            })
        }, interval*2 + 100);

	setInterval(function(){
            el2.animate({
                bottom: '+=' + 5
            }, 500, function(){
                el2.animate({
                    bottom: '-=' + 5
                }, 500)
            })
        }, interval*2 + 100);

	setInterval(function(){
            el3.animate({
                bottom: '+=' + 5
            }, 500, function(){
                el3.animate({
                    bottom: '-=' + 5
                }, 500)
            })
        }, interval*2 + 100);

	setInterval(function(){
            el4.animate({
                bottom: '+=' + 5
            }, 500, function(){
                el4.animate({
                    bottom: '-=' + 5
                }, 500)
            })
        }, interval*2 + 100);

	setInterval(function(){
            el5.animate({
                bottom: '+=' + 5
            }, 500, function(){
                el5.animate({
                    bottom: '-=' + 5
                }, 500)
            })
        }, interval*2 + 100);

    }());

	;(function(){ // клики по li меню
		/*$('.b-menu-item').click(function(e){
			var href = $(this).children('.b-menu-item__href').attr('href');
			document.location.href = href;
		});*/
	})();

	/*;(function(){ // раскрытие/закрытие отзыва по клику
		$('.b-feedback-item__full-view').click(function(){
			var time = 300;
			var _this = $(this);
			var el = _this.parent().siblings('.b-feedback-item__text'), 
				curHeight = 103;
			if (_this.hasClass('active')){
				el.animate({'height':curHeight + 'px'}, time, function(){
					_this.removeClass('active').siblings().removeClass('active');
					if (_this.text() !== ''){
						_this.text('читать весь отзыв');
					}
					else{
						_this.siblings('.b-feedback-item__full-view-text').text('читать весь отзыв');
					}
				});
			}
			else{
				var selfHeight = el.css('height', 'auto').height();
				el.css('height', curHeight+'px');
				el.animate({'height': selfHeight}, time, function(){
					_this.addClass('active').siblings().addClass('active');
					if (_this.text() !== ''){
						_this.text('скрыть');
					}
					else{
						_this.siblings('.b-feedback-item__full-view-text').text('скрыть');
					}
				});
			}
			return false;
		});
	})();*/
	
	;(function(){ // костыль для macов
		if (navigator.platform.toLowerCase().indexOf('mac')+1){
			$('.b-choose-proposal__all, .b-tips__title, .b-tips-change__title, .b-step-1__title, .b-step-1__title, .b-step-2__title, .b-step-2__video-title, .b-step-3__title, .b-step-4__title, .b-step-5__title').addClass('mac'); // тайтлы на главной
		}
		if ($.browser.safari){
			$('.b-choose-proposal-item, .generator, .technology, .how-choose, .useful-help, .user-feedback, .promo-actions').addClass('mac');// меню для сафари 5
		}
	})();

	;(function(){ // стрелки вниз
		var time = 300;
		$('.b-whatis-arrow-block__arrow').click(function(){
			if ($('.b-whatis-block_how').is(':visible')){
				$('html:not(:animated),body:not(:animated)').animate({scrollTop: $('.b-whatis-block_how').offset().top - 125 }, time);
			}
			else{
				$('html:not(:animated),body:not(:animated)').animate({scrollTop: $('.b-whatis-block_key').offset().top - 145 }, time);
			}
			return false;
		});
		$('.b-tech-arrow-block__arrow').click(function(){
			$('html:not(:animated),body:not(:animated)').animate({scrollTop: $('.b-tech__evolution').eq(0).offset().top - 25}, time);
			return false;
		});
	})();
	
	
	;(function(){ // центрирование меню при разрешение меньшем 1900 и большим 990
		var mrgRes = false;
		$(window).resize(function(){
			if ($(window).width() < 1892 && $(window).width() > 990){
				var mrg = ($(window).width() - $('.b-pagination').width() - $('.b-menu-list').width())/2 + $('.b-pagination').width();
				$('.b-menu-list').css('margin-left', mrg+'px');
				mrgRes = true;
			}
			if ($(window).width() >= 1892){
				$('.b-menu-list').css('margin-left', 'auto');
			}
			$('.b-five-steps__arrow').removeAttr('style').stop();
		});
		
		$(document).ready(function(){
			$(window).trigger('resize');
			var mrg = parseInt($('.b-menu-list').css('margin-left'));
			if (mrgRes){
				$('.b-menu-list').css('margin-left', mrg-7+'px');
			}
		});
	})();
})();
$(document).ready(function(){
	mail_popup();
});
function mail_popup(variant){
	var cookieName = 'mail_popup';
	var cookieOptions = {expires: 1, path: '/'};

	if($.cookie(cookieName)=='closed') {$(".popup_mail .popup").css("display","none"); $(".popup_mail").addClass("popup_closed");}
	else {$(".popup_mail .popup").fadeIn(500); $(".popup_mail.popup_closed").removeClass("popup_closed");}

	$(".popup_mail .popup .close").click(function(){
		$(this).parents(".popup").fadeOut(500);
		$(".popup_mail").addClass("popup_closed");
		$.cookie(cookieName, 'closed', cookieOptions);
	});
	$(".popup_mail .girl").click(function(){
		$(this).parents(".popup_mail").find(".popup").fadeIn(500);
		$(".popup_mail.popup_closed").removeClass("popup_closed");
		$.cookie(cookieName, '', cookieOptions);
	});
}